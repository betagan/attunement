
--[[
This file just defines a bunch of stuff provided by aetherflow for
use in the test suite.
--]]

busted = require('busted')

local public = {}

function newGenericElement()
	public.genericElement = {
		Show = busted.spy.new(function() end),
		Hide = busted.spy.new(function() end)
	}
	return public.genericElement
end

UI = {
	NewRectangle = busted.spy.new(function(level) return newGenericElement() end),
	NewLabel = busted.spy.new(function(level) return newGenericElement() end),

	PrimaryMonitorHeight = 100,
	PrimaryMonitorWidth = 100
}

return public
