
fill = require('spec.aetherflow-fill')

require('core')
require('drawable')

describe('drawable', function()
	insulate('wrappers', function()
		it('wraps native elements', function()
			local drawable = Attunement.Rectangle:New()
			local value = 100

			drawable.X = value
			local result = drawable.X

			assert.equals(value, fill.genericElement.X)
			assert.equals(value, result)
		end)

		it('takes parameters in constructor', function()
			local value = 100
			local drawable = Attunement.Rectangle:New{X = value}

			assert.equals(value, fill.genericElement.X)
		end)

		it('sets layer if passed to constructor', function()
			local layer = 'High'
			local drawable = Attunement.Rectangle:New{Layer = layer}

			assert.spy(UI.NewRectangle).was.called()
			assert.spy(UI.NewRectangle).was.called_with(layer)
		end)

		it('prevents invalid attribute access', function()
			local drawable = Attunement.Rectangle:New()

			assert.has_error(function()
				local fake = drawable.fake
			end, 'Attempted to read undefined attribute `fake` on Rectangle')

			assert.has_error(function()
				drawable.fake = 'fake'
			end, 'Attempted to write undefined attribute `fake` on Rectangle')
		end)
	end)
end)
