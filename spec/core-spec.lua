
fill = require('spec.aetherflow-fill')

require('core')

describe('core', function()
	it('is defined', function()
		assert.truthy(Attunement.Core)
	end)

	insulate('OnLoad', function()
		-- Check initial state of OnLoad
		it('defines UI.OnLoad', function()
			assert.is_function(UI.OnLoad)
		end)

		it('is not defined by default', function()
			assert.falsy(Attunement.OnLoad)
		end)

		it('proxies UI.OnLoad', function()
			local s = spy.new(function() end)
			Attunement.OnLoad = s

			UI.OnLoad()

			assert.spy(s).was.called()
		end)

		it('catches user errors', function()
			local s = spy.new(function() error('test error') end)
			Attunement.OnLoad = s

			UI.OnLoad()

			assert.spy(s).was.called()
			assert.spy(UI.NewLabel).was.called()
			assert.spy(UI.NewLabel).was.called_with('Dialog')
			assert.spy(fill.genericElement.Show).was.called()
			assert.matches('test error', fill.genericElement.Text)
		end)
	end)

	insulate('OnFrame', function()
		it('defines UI.OnFrame', function()
			assert.is_function(UI.OnFrame)
		end)

		it('is not defined by default', function()
			assert.falsy(Attunement.OnFrame)
		end)

		-- Function mode
		it('proxies UI.OnFrame', function()
			-- Need to proxy to the spy due to the type check
			local s = spy.new(function() end)
			Attunement.OnFrame = function(...) s(...) end

			local ticks = 12345

			UI.OnFrame(ticks)

			assert.spy(s).was.called()
			assert.spy(s).was.called_with(ticks)
		end)

		it('catches user errors', function()
			local s = spy.new(function() error('test error') end)
			Attunement.OnFrame = function(...) s(...) end
			local ticks = 12345

			UI.OnFrame(ticks)

			assert.spy(s).was.called()
			assert.spy(s).was.called_with(ticks)
			assert.spy(UI.NewLabel).was.called()
			assert.spy(UI.NewLabel).was.called_with('Dialog')
			assert.spy(fill.genericElement.Show).was.called()
			assert.matches('test error', fill.genericElement.Text)
		end)

		-- Table mode
		it('handles a table of functions', function()
			local functions = {
				spy.new(function() end),
				spy.new(function() end),
				spy.new(function() end)
			}
			Attunement.OnFrame = functions
			local ticks = 12345

			UI.OnFrame(ticks)

			for _, s in ipairs(functions) do
				assert.spy(s).was.called()
				assert.spy(s).was.called_with(ticks)
			end
		end)
	end)

	-- Has to be in a seperate block to test not having DS
	insulate('Interval', function()
		it('errors when data-structures is not included', function()
			local s = spy.new(function() end)

			assert.has.errors(function()
				Attunement.Interval:New(100, s)
			end)
			assert.spy(s).was_not.called()
		end)
	end)

	insulate('Interval', function()
		require('data-structures')

		-- Need to destroy the interval data after each run
		after_each(function()
			Attunement.Interval.setUp = false
		end)

		it('handles a basic interval', function()
			local s = spy.new(function() end)
			Attunement.Interval:New(100, s)

			UI.OnFrame(0)
			assert.spy(s).was_not.called()

			UI.OnFrame(50)
			assert.spy(s).was_not.called()

			UI.OnFrame(100)
			assert.spy(s).was.called(1)

			UI.OnFrame(150)
			assert.spy(s).was.called(1)

			UI.OnFrame(200)
			assert.spy(s).was.called(2)
		end)

		it('repeats to make up for missed intervals', function()
			local s = spy.new(function() end)
			Attunement.Interval:New(10, s)

			UI.OnFrame(0)
			assert.spy(s).was_not.called()

			UI.OnFrame(100)
			assert.spy(s).was.called(10)
		end)

		it('handles multiple intervals', function()
			local s1 = spy.new(function() end)
			local s2 = spy.new(function() end)

			Attunement.Interval:New(30, s1)
			Attunement.Interval:New(100, s2)

			UI.OnFrame(0)
			assert.spy(s1).was_not.called()
			assert.spy(s2).was_not.called()

			UI.OnFrame(50)
			assert.spy(s1).was.called(1)
			assert.spy(s2).was_not.called()

			UI.OnFrame(100)
			assert.spy(s1).was.called(3)
			assert.spy(s2).was.called(1)

			UI.OnFrame(150)
			assert.spy(s1).was.called(5)
			assert.spy(s2).was.called(1)
		end)
	end)
end)
