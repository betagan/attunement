
Debug `attunement/debug.lua`
============================

Utilities to ease addon development and debugging.

Depends on:
* `attunement/drawable.lua`

Contents:
* [Console](#console)

Console
-------

Attunement supplies a debugging console for use when developing an addon. The console is hidden by default, but may be enabled from within addon code.

Should an error be caught by one of Attunement's wrapper functions, and the debug module is included, the console will automatically be enabled and used to display the error to the user - useful for debugging errors from users and the like.

### Basic Usage

```lua
console = Attunement.Debug.Console:Enable()
console:Write('A Debug Message')
Attunement.Console:Write('Another debug message')
```

### Function Reference

**print(...)**

When the console is enabled, the global `print` function is re-defined to print to the console. It should behave akin to the reference Lua implementation's `print` function. `print` will output all messages at log level `debug` (see `:write`).

**Attunement.Debug.Console:Enable()**

Sets up the console and returns a reference reference to itself. If the console has already been enabled, will simply return the reference.

The console acts as a singleton, and can be called using either the reference or the full path, as shown in the basic usage.

**Attunement.Debug.Console:Write(...)**

If the console is not enabled at the time, this function will do nothing. Otherwise, will add each argument passed to the console (on a new line).

Optionally, if the first argument is one of `{debug, info, warning, error}`, the message will be output with that log level. If not specified, `debug` will be assumed.

**Attunement.Debug.Console:Clear()**

Clears the console, for whatever reason you'd want to do that.
