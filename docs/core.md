
Core `attunement/core.lua`
==========================

The core component of Attunement, required when using ***any*** other Attunement components. For best results, make sure core is loaded before other Attunement files in your `.ainf` file. Technically shouldn't make any difference, but just to be on the safe side.

* [Main Functions](#main-functions)
* [Interval & Timers](#intervals-and-timers)

Main Functions
--------------

Attunement provides wrappers around Aetherflow's `UI` function hooks, that allows the library to automatically handle the dirty work for you. When using Attunement, **make sure** you use these wrapper functions. Failure to do so may result in some pretty funky undefined behaviour.

All of these wrappers execute with error handling. Should something go wrong, a nasty red error message will be displayed to the user, or alternatively an error message will be printed to the [attunement console](docs/debug.md#console) if it is enabled.

### Attunement.OnLoad()

Just like `UI.OnLoad`, this function will be run when Aetherflow's injection is complete, and before any `OnFrame` calls.

`OnLoad` is best used to set up any primary drawables and initial values required by your addon.

### Attunement.OnFrame(ticks)

As with `UI.OnLoad`, this may be set to a function that will be called every frame update, with `ticks` corresponding to the number of milliseconds since system start.

*However*, `Attunement.OnFrame` may also be set to a table value. When defined as a table, each value in the table will be called each frame.

If using a table for `OnFrame`, be mindful that each `OnFrame` function is self-contained. Due to the nature of Lua's implementation of tables, the order of execution for these functions ***is not guaranteed*** in any way.

Intervals and timers
--------------------

Also included in the `core` module is the `Interval` system, which allows you to specify a function that will be executed at a set interval (funny that).

Currently, the system is fully functional but lacks a degree of functionality, however this will be incorporated as part of future additions.

### Attunement.Interval:New(interval, func)

Registers a new interval to execute `func` every `interval` milliseconds. The function is guaranteed to be run as close to the interval as possible, even if that would potentially involve running multiple times per frame. As such, setting `interval = 1` is... inadvisable.

Notes:
* `func` is currently called with no arguments. A current `tick` argument (a-la `OnFrame`) will likely be added in the near future.
* An option will be added to limit `func` executions to one per frame.
* There is currently no way of *stopping* an interval once it's started. This is planned functionality, and will be added soon.
* As above, there is no way to set a simple timeout - a delayed call execution without repetition. Again, will be added soon.

Ergh, lots of features to add. Soon™.
