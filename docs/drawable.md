
Drawable `attunement/drawable.lua`
==================================

Wrappers around the lower-level rendered elements exposed by Aetherflow that provide a (somewhat) nicer interface.

Objects:
* [Element](#element)
* [Rectangle](#rectangle)
* [Ellipse](#ellipse)
* [Label](#label)
* [Texture](#texture)


Element
-------

All of attunement's drawables wrap aetherflow's native elements, providing a less error-prone and nicer-to-user interface.

The following function reference and valid attribute index is shared by all element types in attunement.

### Function Reference

**Attunement.<Element>:New(params)**

Creates a new <Element> (refer to following documentation for valid element types), optionally initialised with the passed parameters. If passed, parameters should be a table of [valid attributes](#valid-attributes) to initiate the element with.

**element:Show()**

Alias for `element.Hidden = false`.

**element:Hide()**

Alias for `element.Hidden = true`.

### Valid Attributes

Due to how aetherflow handles element creation and modification, setting an invalid attribute on a native Element will cause the framework to crash. Attunement's element wrappers prevent these crashes, and raise lua errors instead, however it's probably best not to go around throwing errors willy nilly, so here's a list of globally valid attributes:

```lua
attributes = {
	-- Basic Positioning
	X, Y, Width, Height,

	-- Transformations
	ScaleCenterX, ScaleCenterY, ScaleX, ScaleY, ScalingRotation,
	SkewX, SkewY,
	RotationCenterX, RotationCenterY, Rotation,
	TranslationX, TranslationY

	Hidden,

	-- Colouring
	Color,
	BorderWidth, BorderColor,
	DrawBackground, BackgroundColor,

	-- Event callbacks
	OnMouseLeftDown, OnMouseLeftUp,
	OnMouseRightDown, OnMouseRightUp,
	OnMouseMiddleDown, OnMouseMiddleUp,
	OnMouseEnter, OnMouseLeave, OnMouseWheel,

	-- Drag
	Draggable,DragMinX, DragMaxX, DragMinY, DragMaxY,

	-- Focus
	Focuseable, HasFocus
}
```

Rectangle
---------

Rectangle is a very basic element type, and only accepts the core element attributes.

Ellipse
-------

Ellipse allows the following additonal attributes:

```lua
ellipseAttributes = {
	CenterX, CenterY,
	RadiusX, RadiusY
}
```

Label
-----

Label allows the following additional attributes:

```lua
labelAttributes = {
	Text,
	FontFamily, FontWeight, FontSize, FontStyle, FontStretch,
		FlowDirection, ReadingDirection,
		ParagraphAlignment, TextAlignment,
		WordWrapping
}
```

In addition, Label also exposes constants for several of its variables:

```lua
labelConstants = {
	-- FlowDirection
	FLOW_TOP_TO_BOTTOM,
	FLOW_BOTTOM_TO_TOP,
	FLOW_LEFT_TO_RIGHT,
	FLOW_RIGHT_TO_LEFT,

	-- ReadingDirecction
	READ_LEFT_TO_RIGHT,
	READ_RIGHT_TO_LEFT,
	READ_TOP_TO_BOTTOM,
	READ_BOTTOM_TO_TOP,

	-- ParagraphAlignment
	PARAGRAPH_NEAR,
	PARAGRAPH_FAR,
	PARAGRAPH_CENTER,

	-- TextAlignment
	ALIGN_LEADING,
	ALIGN_TRAILING,
	ALIGN_CENTER,
	ALIGN_JUSTIFIED,

	-- WordWrapping
	WRAP,
	NO_WRAP,
	WRAP_EMERGENCY_BREAK,
	WRAP_WHOLE_WORD,
	WRAP_CHARACTER
}
```

Texture
-------

Texture allows the following additional attributes:

```lua
textureAttributes = {
	FilePath,
	DrawTint
}
```

