
-- Ensure Attunement is defined
if not Attunement then
	Attunement = {}
end

Attunement.Core = {}

-- Define print() as a noop if it's not already been defined, simply so things don't error out
if not print then
	print = function() end
end

--[[
Error Handling
--]]

-- Used for outputting errors when debug module is not included
local errorText
local getBasicErrorLabel = function()
	if not errorText then
		errorText = UI.NewLabel('Dialog')
		errorText.Color = "#FFFF0000"
		errorText.Width = UI.PrimaryMonitorWidth
		errorText:Show()
	end

	return errorText
end

local handleError = function(err)
	-- If the debug console is enabled, pop it up and show the error
	if Attunement.Debug and Attunement.Debug.Console then
		Attunement.Debug.Console:Enable():Write('error', err)

	-- Else, generate a nasty red text so the user at least knows something went wrong
	else
		output = getBasicErrorLabel()
		output.Text = err
	end
end

--[[
Wrappers for OnLoad / OnFrame
--]]

-- Real OnLoad
UI.OnLoad = function()
	if Attunement.OnLoad then
		local status, err = pcall(Attunement.OnLoad)
		if not status then
			handleError(err)
		end
	end
end

-- Real OnFrame
UI.OnFrame = function(ticks)
	local status, err = pcall(function()
		-- No need to check if OnFrame exists, type() returns nil if it doesn't
		local onFrameType = type(Attunement.OnFrame)

		-- If OnFrame is a function, just call it
		if onFrameType == 'function' then
			Attunement.OnFrame(ticks)

		-- OnFrame is a table, call all values
		elseif onFrameType == 'table' then
			for _, func in pairs(Attunement.OnFrame) do
				func(ticks)
			end
		end
	end)

	-- There was an error, throw it.
	if not status then
		handleError(err)
	end
end

--[[
Interval and timeout timers
--]]

Attunement.Interval = {
	setUp = false
}
function Attunement.Interval:New(interval, func)
	-- Ensure the Interval stuff has been set up
	self:SetUp()

	-- Build the schedule task and queue it to be queued (hue)
	local task = {
		func = func,
		interval = interval
	}
	self.add:Push(task)
end

function Attunement.Interval:SetUp()
	-- If we've already set outselves up, do nothing
	if self.setUp then return end

	local DS = Attunement.DataStructures

	-- Error out if data-structures isn't available
	if not DS then
		error('OnInterval requires `data-structures.lua` to be loaded.')
	end

	-- Set up all the fields we need
	self.add = DS.FIFOQueue:New()
	-- TODO: Probably need a to-remove queue, too
	self.queue = DS.PriorityQueue:New()
	self.nextPriority = math.huge

	-- Register the OnFrame handler with the rest of attunement
	-- TODO: Currently have to proxy to keep reference to self. Better workaround?
	table.insert(Attunement.OnFrame, function(ticks) self:OnFrame(ticks) end)

	self.setUp = true
end

function Attunement.Interval:OnFrame(ticks)
	-- Add pending tasks if there are any
	while not self.add:Empty() do
		local task = self.add:Pop()
		local priority = ticks + task.interval
		task.priority = priority
		self.queue:Push(task)

		-- Keep track of the next priority that's going to be popped
		if priority < self.nextPriority then
			self.nextPriority = priority
		end
	end

	-- If there's nothing queued, quit out without trying to pop
	if self.nextPriority > ticks then
		return
	end

	-- Something's gonna get popped, so start popping
	while true do
		local task = self.queue:Pop()

		-- No tasks remaining, kill the loop
		if not task then
			self.nextPriority = math.huge
			break
		end

		-- Task is for future, push it back on and update the next priority
		if task.priority > ticks then
			self.nextPriority = task.priority
			self.queue:Push(task)
			break
		end

		-- Task needs executing, so call it
		task.func() -- TODO: Pass variables? Iuno.

		-- TODO: Allow single-execution calls?
		-- TODO: Allow option between tick+interval and priority+interval
		-- Update the task's priority and add back into the queue
		task.priority = task.priority + task.interval
		self.queue:Push(task)
	end
end
