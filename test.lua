
Attunement.OnFrame = {}
Attunement.OnLoad = function()
	-- Display a default rect at 200,200
	rect = Attunement.Rectangle:New{
		X = 600,
		Y = 200,
		Layer = 'Tooltip'
	}
	rect:Show()

	-- print() is a noop
	print('This will go nowhere')

	-- Enable the console
	local console = Attunement.Debug.Console:Enable()
	console:Write('This was a triumph!', 'I\'m making a note here:')

	-- Generate some data for the console
	table.insert(Attunement.OnFrame, function(ticks)
		Attunement.Debug.Console:Clear()
		-- Now that the console has been enabled, print() is a synonym for console:write()
		print('Huge success!', 'It\'s hard to overstate my satisfaction.')
		-- Printing ticks will tell us its still running
		print("ticks", ticks)
		-- You can loop Elements
		for attr, value in rect:Attributes() do
			print("rect["..attr.."] = "..tostring(value))
		end
		-- Will throw an error to the console (bad access)
		rect.x = 300
	end)
end
