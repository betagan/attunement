
-- Ensure Attunement is defined
if not Attunement then
	Attunement = {}
end

--[[
Wrappers
--]]

-- All AF elements inherit these attributes, so no need to repeat them for each indivitual wrapper
local getElementAttributes = function(extraAttrs)
	-- Base attributes used on everything
	local attributes = {
		-- Basic Positioning
		X=1, Y=1, Width=1, Height=1,

		-- Transformations
		ScaleCenterX=1, ScaleCenterY=1, ScaleX=1, ScaleY=1, ScalingRotation=1,
		SkewX=1, SkewY=1,
		RotationCenterX=1, RotationCenterY=1, Rotation=1,
		TranslationX=1, TranslationY=1,

		Hidden=1,

		-- Colouring
		Color=1,
		BorderWidth=1, BorderColor=1,
		DrawBackground=1, BackgroundColor=1,

		-- Event callbacks
		OnMouseLeftDown=1, OnMouseLeftUp=1,
		OnMouseRightDown=1, OnMouseRightUp=1,
		OnMouseMiddleDown=1, OnMouseMiddleUp=1,
		OnMouseEnter=1, OnMouseLeave=1, OnMouseWheel=1,

		-- Drag
		Draggable=1,DragMinX=1, DragMaxX=1, DragMinY=1, DragMaxY=1,

		-- Focus
		Focuseable=1, HasFocus=1
	}

	if extraAttrs then
		for key, value in pairs(extraAttrs) do attributes[key] = value end
	end

	return attributes
end

-- Names of the available elements, for automatically handled wrapping
-- Arg list is stored as it is to allow hash lookups (as opposed to linear
-- lookups) to optimise performance.
local elements = {
	Rectangle = getElementAttributes(),

	Ellipse = getElementAttributes{
		CenterX=1, CenterY=1,
		RadiusX=1, RadiusY=1
	},

	Label = getElementAttributes{
		Text=1,
		FontFamily=1, FontWeight=1, FontSize=1, FontStyle=1, FontStretch=1,
		FlowDirection=1, ReadingDirection=1,
		ParagraphAlignment=1, TextAlignment=1,
		WordWrapping=1
	},

	Texture = getElementAttributes{
		FilePath=1,
		DrawTint=1
	}
}

-- For each of the defined elements, wrap and add a nice constructor
for name, args in pairs(elements) do
	Attunement[name] = {
		New = function(parent, params)
			local self = {}

			-- Get the layer if passed, defaulting to medium
			local layer = 'Medium'
			if params and params.Layer then
				layer = params.Layer
				params.Layer = nil
			end

			-- Create the element in AF
			local elem = UI['New' .. name](layer)

			-- Make forwarding versions of Show and Hide
			function self:Hide() elem:Hide() end
			function self:Show() elem:Show() end

			-- Allow Iteration of the attributes
			local function AttributeIteration(_, index)
				index, _ = next(args,index)
				if index then
					return index, self[index]
				end
			end
			function self:Attributes()
				return AttributeIteration, nil, nil
			end

			-- Proxy the table so we can catch bad assigments before they crash Aetherflow
			local mt = {
				__index = function(table, key)
					if args[key] then
						return elem[key]
					else
						error('Attempted to read undefined attribute `' .. tostring(key) .. '` on ' .. name, 2)
					end
				end,

				__newindex = function(table, key, value)
					if args[key] then
						elem[key] = value
					else
						error('Attempted to write undefined attribute `' .. tostring(key) .. '` on ' .. name, 2)
					end
				end
			}
			setmetatable(self, mt)

			-- Set all the params into the new element
			-- This should already be using the proxy code
			-- So even it can throw errors.
			if params then
				for key, value in pairs(params) do self[key] = value end
			end

			return self
		end
	}
end

-- Elements have a bunch of parameters that take obscure numbers for settings. Expose some nicer constants.
local elementConstants = {
	Label = {
		-- FlowDirection
		FLOW_TOP_TO_BOTTOM = 0,
		FLOW_BOTTOM_TO_TOP = 1,
		FLOW_LEFT_TO_RIGHT = 2,
		FLOW_RIGHT_TO_LEFT = 3,

		-- ReadingDirecction
		READ_LEFT_TO_RIGHT = 0,
		READ_RIGHT_TO_LEFT = 1,
		READ_TOP_TO_BOTTOM = 2,
		READ_BOTTOM_TO_TOP = 3,

		-- ParagraphAlignment
		PARAGRAPH_NEAR   = 0,
		PARAGRAPH_FAR    = 1,
		PARAGRAPH_CENTER = 2,

		-- TextAlignment
		ALIGN_LEADING   = 0,
		ALIGN_TRAILING  = 1,
		ALIGN_CENTER    = 2,
		ALIGN_JUSTIFIED = 3,

		-- WordWrapping
		WRAP                 = 0,
		NO_WRAP              = 1,
		WRAP_EMERGENCY_BREAK = 2,
		WRAP_WHOLE_WORD      = 3,
		WRAP_CHARACTER       = 4
	}
}
for element, constants in pairs(elementConstants) do
	for key, value in pairs(constants) do
		Attunement[element][key] = value
	end
end
